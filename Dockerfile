# syntax=docker/dockerfile:1

FROM registry.suse.com/bci/bci-base:15.4 AS salt_master

LABEL org.opencontainers.image.source https://gitlab.com/School_Guy/docker-salt
LABEL org.opencontainers.image.title Salt
LABEL org.opencontainers.image.description "Saltstack configuration management OCI based on SUSE BCI."
LABEL org.opencontainers.image.author Enno Gotthold <matrixfuller@4seul.de>

COPY entrypoint.py /
RUN zypper in -y salt=3004 && zypper clean

EXPOSE 4505 4506
VOLUME /srv/salt
VOLUME /etc/salt

CMD [ "/entrypoint.py" ]

FROM salt_master as salt_api

RUN zypper in -y salt-api=3004 && zypper clean

EXPOSE 8000

FROM salt_api

VOLUME /srv/susemanager/salt
