[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![linting: pylint](https://img.shields.io/badge/linting-pylint-yellowgreen)](https://github.com/PyCQA/pylint)
[![linting: hadolint](https://img.shields.io/badge/linting-hadolint-brightgreen)](https://github.com/hadolint/hadolint)

# docker-salt

This project aims to provide more complete experience when hosting your Salt Master (and API) in a container.

## How to use this image?

This image is fully compatible in the usage to the upstream image. It should be a drop-in replacement.

```
docker run -d schoolguy/salt
```

## What ENV variables can be used to configure this image?

- `MODE`:
  - Allowed values: `master`, `api`, `minion`, `proxy`, `master-api`
  - Default: `master-api`
  - Description: Decides what processes will be started inside the container by the entrypoint script.
  - Note: `master-api` will be deprecated and removed as soon as Salt upstream doesn't require that the Salt API and
    Master share the same host.
- `SALT_API_CONFIG`:
- `SALT_MASTER_CONFIG`:
- `SALT_PROXY_ID`
- `SALT_PROXY_CONFIG`
- `SALT_MINION_CONFIG`
- `TODO`

## How to use this image in Kubernetes?

There is currently no salt-operator available and no HelmChart. If this repository is successful enough, then both an
Operator and a HelmChart will be created.

## Repository layout

Due to the limitation in the openSUSE Build Service project this repository can only have a single level. All files
need to live inside the root repository layout.

## Relation to Upstream Saltstack

The offical upstream container can be found at the following places:

- [Gitlab saltstack/open/saltdocker](https://gitlab.com/saltstack/open/saltdocker)
- [Docker Hub](https://hub.docker.com/r/saltstack/salt)

This project is not affilated with Saltstack or VMware in any way.

## How to build this image in OBS?

Project Meta:

```
<project name="Project:Path">
  <title>Project Name</title>
  <description>Project Description</description>
  <person userid="YourUsername" role="maintainer"/>
  <build>
    <enable/>
  </build>
  <publish>
    <enable/>
  </publish>
  <repository name="images">
    <path project="SUSE:Registry" repository="standard"/>
    <path project="SUSE:SLE-15-SP4:Update" repository="standard"/>
    <arch>x86_64</arch>
  </repository>
  <repository name="containers">
    <path project="SUSE:Templates:Images:SLE-15-SP4" repository="containers"/>
    <arch>x86_64</arch>
  </repository>
</project>
```

Project Config:

```
Prefer: openSUSE-release
Prefer: SLES_BCL-release

%if "%_repository" == "images" || "%_repository" == "images_arm" || "%_repository" == "images_ppc"
Type: kiwi
Repotype: none
Patterntype: none
%endif

%if "%_repository" == "containers" || "%_repository" == "containers_arm" || "%_repository" == "containers_ppc"
Type: docker
Repotype: none
Patterntype: none
BuildEngine: podman
%endif
```
